// console.log("Hello World");


// 1.  Create a function which will be able to add two numbers.
		// -Numbers must be provided as arguments.
		// -Display the result of the addition in our console.
		// -function should only display result. It should not return anything



// Create a function which will be able to subtract two numbers.
// 		-Numbers must be provided as arguments.
// 		-Display the result of subtraction in our console.
// 		-function should only display result. It should not return anything.

// 		-invoke and pass 2 arguments to the addition function
// 		-invoke and pass 2 arguments to the subtraction function


// Addition:

function addNumbers(num1a, num2a){
	let sumOfNumbers = num1a + num2a; 
	console.log("Displayed sum of " + num1a + " and " + num2a);
	console.log(sumOfNumbers);
}

addNumbers(5, 15);


// Subtraction:

function subtractNumbers(num1s, num2s){
	let differenceOfNumbers = num1s - num2s; 
	console.log("Displayed difference of " + num1s + " and " + num2s);
	console.log(differenceOfNumbers);
}

subtractNumbers(20, 5);



// 2.  Create a function which will be able to multiply two numbers.
// 			-Numbers must be provided as arguments.
// 			-Return the result of the multiplication.

// 		Create a function which will be able to divide two numbers.
// 			-Numbers must be provided as arguments.
// 			-Return the result of the division.

// 	 	Create a global variable called outside of the function called product.
// 			-This product variable should be able to receive and store the result of multiplication function.
// 		Create a global variable called outside of the function called quotient.
// 			-This quotient variable should be able to receive and store the result of division function.

// 		Log the value of product variable in the console.
// 		Log the value of quotient variable in the console.


// Multiplication:

function multiplyNumbers(num1p, num2p){
	let productOfNumbers = num1p * num2p;
	return productOfNumbers;
	
}

let multiplicationAnswer = multiplyNumbers(50, 10);

console.log("The product of  50 and 10 : ");
console.log(multiplicationAnswer);



// Division:

function divideNumbers(num1d, num2d){
	let quotientOfNumbers = num1d * num2d;
	return quotientOfNumbers;
	
}

let divisionAnswer = divideNumbers(50, 10);

console.log("The quotient of  50 and 10 : ");
console.log(divisionAnswer);


// 3. 	Create a function which will be able to get total area of a circle from a provided radius.
// 			-a number should be provided as an argument.
// 			-look up the formula for calculating the area of a circle with a provided/given radius.
// 			-look up the use of the exponent operator.
// 			-you can save the value of the calculation in a variable.
// 			-return the result of the area calculation.

// 		Create a global variable called outside of the function called circleArea.
// 			-This variable should be able to receive and store the result of the circle area calculation.

// 	Log the value of the circleArea variable in the console.


// Area of circle: A=πr2

function circleArea(radius){
	let areaOfACircle = 3.1416 * (radius) ** 2;
	return areaOfACircle;
	
}

let areaAnswer = circleArea(15);
console.log("The result of getting the area of a circle with 15 radius");
console.log(areaAnswer);


// 4. 	Create a function which will be able to get total average of four numbers.
// 			-4 numbers should be provided as an argument.
// 			-look up the formula for calculating the average of numbers.
// 			-you can save the value of the calculation in a variable.
// 			-return the result of the average calculation.

// 	    Create a global variable called outside of the function called averageVar.
// 			-This variable should be able to receive and store the result of the average calculation
// 			-Log the value of the averageVar variable in the console.


// Average

function getAverage (n1, n2, n3, n4){
	let arr = [n1, n2, n3, n4]; var sum = 0;
	arr. forEach((num) => { sum += num }); 
	average = sum / arr. length; 
	return average;

}

	 let averageAnswer = getAverage(20, 40, 60, 80);

	console.log("The average of 20, 40, 60 and 80 : ");
	console. log(averageAnswer);



// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
// 			-this function should take 2 numbers as an argument, your score and the total score.
// 			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
// 			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
// 			-return the value of the variable isPassed.
// 			-This function should return a boolean.

// 		Create a global variable called outside of the function called isPassingScore.
// 			-This variable should be able to receive and store the boolean result of the checker function.
// 			-Log the value of the isPassingScore variable in the console.




// Percentage

function percentComputation(yourScore, totalScore){
	let percentageOfNumbers = (yourScore / totalScore)*100;
	return percentageOfNumbers;
	
}

let percentageAnswer = percentComputation(38, 50);
console.log("Is 38/50 a passing score?");
console.log(percentageAnswer >= 0.75);